module wavelet_mod
  use shared_mod
  use domain_mod
  use comm_mpi_mod
  implicit none

  type(Float_Field), target :: wav_coeff(S_HEIGHT:S_VELO)
  real(8), dimension(9) :: Iu_Base_Wgt

contains
  subroutine init_wavelet_mod()
      logical :: initialized = .False.
      if (initialized) return ! initialize only once
      call init_shared_mod()
      call init_domain_mod()
      Iu_Base_Wgt = (/16.0_8, -1.0_8, 1.0_8, 1.0_8, -1.0_8, -1.0_8, -1.0_8, 1.0_8, 1.0_8/)/16.0_8
      initialized = .True.
  end subroutine init_wavelet_mod

  subroutine IWT_interp_vel_penta(dom, p, c, offs, dims)
      type(Domain) dom
      integer p
      integer c
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,9) :: dims
      integer p_chd
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,9) :: dims_chd
      integer id_chd
      integer idN_chd
      real(8) v
      integer idE_chd
      real(8), dimension(2) :: u
      p_chd = dom%patch%elts(p+1)%children(c-4)
      if (p_chd .eq. 0) return
      call get_offs_Domain(dom, p_chd, offs_chd, dims_chd)
      if (c .eq. IMINUSJPLUS) then
          id_chd = idx(0, LAST - 1, offs_chd, dims_chd)
          idN_chd = idx(0, LAST, offs_chd, dims_chd)
          v = (Iu_Base_Wgt(8) + dble(dom%I_u_wgt%elts(idN_chd+1)%enc(8)))*( &
                  velo(idx(0, PATCH_SIZE, offs, dims)*EDGE + UP + 1) &
                 +velo(idx(-1, PATCH_SIZE, offs, dims)*EDGE + RT + 1))
          velo(EDGE*id_chd+UP+1) = velo(EDGE*id_chd+UP+1) - v
          velo(EDGE*idN_chd+UP+1) = velo(EDGE*idN_chd+UP+1) + v
      else
          if (c .eq. IPLUSJMINUS) then
              id_chd = idx(LAST - 1, 0, offs_chd, dims_chd)
              idE_chd = idx(LAST, 0, offs_chd, dims_chd)
              v = -(Iu_Base_Wgt(7) + dble(dom%I_u_wgt%elts(idE_chd+1)%enc(7)))*( &
                      velo(idx(PATCH_SIZE, 0, offs, dims)*EDGE + RT + 1) &
                     +velo(idx(PATCH_SIZE,-1, offs, dims)*EDGE + UP + 1))
              velo(EDGE*id_chd+RT+1) = velo(EDGE*id_chd+RT+1) - v
              velo(EDGE*idE_chd+RT+1) = velo(EDGE*idE_chd+RT+1) + v
          end if
      end if
      if (.not. c .eq. IJMINUS) then
          return
      end if
      id_chd = idx(0, 0, offs_chd, dims_chd)
      idN_chd = idx(0, 1, offs_chd, dims_chd)
      idE_chd = idx(1, 0, offs_chd, dims_chd)
      u = vel_interp_penta_corr(dom, offs, dims, offs_chd, dims_chd)
      velo(EDGE*id_chd+UP+1) = velo(EDGE*id_chd+UP+1) - u(1)
      velo(EDGE*idN_chd+UP+1) = velo(EDGE*idN_chd+UP+1) + u(1)
      velo(EDGE*id_chd+RT+1) = velo(EDGE*id_chd+RT+1) - u(2)
      velo(EDGE*idE_chd+RT+1) = velo(EDGE*idE_chd+RT+1) + u(2)
  end subroutine

  subroutine IWT_interpolate_u_inner(dom, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      integer idNE_chd
      real(8), dimension(6) :: u_inner
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      u_inner = I_u_inner(dom, i_par, j_par, offs_par, dims_par, i_chd, j_chd, &
              offs_chd, dims_chd, EDGE*idE_chd + UP, DG + EDGE*idE_chd, &
              EDGE*idNE_chd + RT, EDGE*idN_chd + RT, DG + EDGE*idN_chd, &
              EDGE*idNE_chd + UP)
      velo(EDGE*idE_chd+UP+1) = u_inner(1) + wc_u(EDGE*idE_chd+UP+1)
      velo(DG+EDGE*idE_chd+1) = u_inner(2) + wc_u(DG+EDGE*idE_chd+1)
      velo(EDGE*idNE_chd+RT+1) = u_inner(3) + wc_u(EDGE*idNE_chd+RT+1)
      velo(EDGE*idN_chd+RT+1) = u_inner(4) + wc_u(EDGE*idN_chd+RT+1)
      velo(DG+EDGE*idN_chd+1) = u_inner(5) + wc_u(DG+EDGE*idN_chd+1)
      velo(EDGE*idNE_chd+UP+1) = u_inner(6) + wc_u(EDGE*idNE_chd+UP+1)
  end subroutine

  function coords_to_rowd(midpt, dirvec, x, y)
      real(8), dimension(6) :: coords_to_rowd
      type(Coord) midpt
      type(Coord) dirvec
      type(Coord) x
      type(Coord) y
      real(8) u
      real(8) v
      real(8), dimension(2) :: xy
      call normalize2(coord2local(dirvec, x, y), u, v)
      xy = coord2local(midpt, x, y)
      coords_to_rowd = (/u, u*xy(1), u*xy(2), v, v*xy(1), v*xy(2)/)
      return
  end function

  function I_u_inner(dom, i_par, j_par, offs_par, dims_par, i_chd, j_chd, &
          offs_chd, dims_chd, idE_UP, idE_DG, idNE_RT, idN_RT, idN_DG, idNE_UP)
      real(8), dimension(6) :: I_u_inner
      type(Domain) dom
      integer i_par
      integer j_par
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer idE_UP
      integer idE_DG
      integer idNE_RT
      integer idN_RT
      integer idN_DG
      integer idNE_UP
      real(8), dimension(6) :: u
      integer id_par
      real(8), dimension(2) :: omega
      integer k
      integer id1_par
      integer id2_par
      integer id
      integer idN
      integer idUP
      integer idDG
      integer idRT
      integer idE
      integer idNE
      integer idN2E
      integer id2NE
      integer idN2
      integer idE2
      u = 0.0_8
      id_par = idx(i_par, j_par, offs_par, dims_par)
      omega = 0.0_8
      do k = 1, 2
          id1_par = idx(i_par - k + 2, j_par, offs_par, dims_par)
          id2_par = idx(i_par, j_par + k - 1, offs_par, dims_par)
          omega(k) = &
                  1.0_8/dom%triarea%elts(TRIAG*id_par+k)*(velo(DG+EDGE*id_par+1)*dom%len%elts(DG+EDGE*id_par+1) &
                  + &
                  velo(EDGE*id1_par+UP+1)*dom%len%elts(EDGE*id1_par+UP+1) &
                  + &
                  velo(EDGE*id2_par+RT+1)*dom%len%elts(EDGE*id2_par+RT+1))
      end do
      id = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idUP = EDGE*id + UP
      idDG = EDGE*id + DG
      idRT = EDGE*id + RT
      idE = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      idN2E = idx(i_chd + 2, j_chd + 1, offs_chd, dims_chd)
      id2NE = idx(i_chd + 1, j_chd + 2, offs_chd, dims_chd)
      idN2 = idx(i_chd, j_chd + 2, offs_chd, dims_chd)
      idE2 = idx(i_chd + 2, j_chd, offs_chd, dims_chd)
      u(1) = (dom%triarea%elts(LORT+TRIAG*id+1)*omega(1) - &
              velo(idRT+1)*dom%len%elts(idRT+1) - &
              velo(idDG+1)*dom%len%elts(idDG+1))/dom%len%elts(idE_UP+1)
      u(2) = (dom%triarea%elts(LORT+TRIAG*idE+1)*omega(1) - &
              velo(EDGE*idE+RT+1)*dom%len%elts(EDGE*idE+RT+1) - &
              velo(EDGE*idE2+UP+1)*dom%len%elts(EDGE*idE2+UP+1))/dom%len%elts(idE_DG+1)
      u(3) = (dom%triarea%elts(LORT+TRIAG*idNE+1)*omega(1) - &
              velo(DG+EDGE*idNE+1)*dom%len%elts(DG+EDGE*idNE+1) - &
              velo(EDGE*idN2E+UP+1)*dom%len%elts(EDGE*idN2E+UP+1))/dom%len%elts(idNE_RT+1)
      u(4) = (dom%triarea%elts(TRIAG*id+UPLT+1)*omega(2) - &
              velo(idUP+1)*dom%len%elts(idUP+1) - &
              velo(idDG+1)*dom%len%elts(idDG+1))/dom%len%elts(idN_RT+1)
      u(5) = (dom%triarea%elts(TRIAG*idN+UPLT+1)*omega(2) - &
              velo(EDGE*idN+UP+1)*dom%len%elts(EDGE*idN+UP+1) - &
              velo(EDGE*idN2+RT+1)*dom%len%elts(EDGE*idN2+RT+1))/dom%len%elts(idN_DG+1)
      u(6) = (dom%triarea%elts(TRIAG*idNE+UPLT+1)*omega(2) - &
              velo(DG+EDGE*idNE+1)*dom%len%elts(DG+EDGE*idNE+1) - &
              velo(EDGE*id2NE+RT+1)*dom%len%elts(EDGE*id2NE+RT+1))/dom%len%elts(idNE_UP+1)
      I_u_inner = u
      return
  end function

  subroutine forward_wavelet_transform()
      integer l, d
      do l = level_end - 1, level_start - 1, -1
          call update_bdry(sol(S_HEIGHT), l+1)
          do d = 1, n_domain(rank+1)
              height => sol(S_HEIGHT)%data(d)%elts
              wc_h => wav_coeff(S_HEIGHT)%data(d)%elts
              call apply_interscale_d(cpt_height_wc, grid(d), l, 0, 0)
              nullify(wc_h, height)
          end do
          call update_bdry(wav_coeff(S_HEIGHT), l+1)
          call apply_interscale(restrict_p, l, 0, 1) ! +1 to include poles

          call apply_interscale(restrict_u, l, 0, 0)
      end do
      sol(:)%bdry_uptodate = .False.
      wav_coeff(S_HEIGHT)%bdry_uptodate = .False.

      call update_bdry(sol(AT_EDGE), NONE)
      do l = level_end - 1, level_start - 1, -1
          do d = 1, n_domain(rank+1)
              wc_u => wav_coeff(S_VELO)%data(d)%elts
              velo => sol(S_VELO)%data(d)%elts
              call apply_interscale_d(cpt_velo_wc, grid(d), l, 0, 0)
              call apply_to_penta_d(cpt_vel_wc_penta, grid(d), l)
              nullify(wc_u, velo)
          end do
      end do
      wav_coeff(S_VELO)%bdry_uptodate = .False.
  end subroutine

  subroutine invers_wavelet_transform(sca_coeff, l_start0)
      type(Float_Field), target :: sca_coeff(S_HEIGHT:S_VELO)
      integer, optional :: l_start0
      integer l, d, k, l_start

      if (present(l_start0)) then; l_start = l_start0; else; l_start = level_start; end if
      do k = S_HEIGHT, S_VELO
          call update_bdry1(wav_coeff(k), level_start, level_end)
          call update_bdry1(sca_coeff(k), l_start, level_end)
      end do

!write(*,*) 'check-p'
!call apply_interscale(check_p, 1, 0, 0)
      sca_coeff(:)%bdry_uptodate = .False.
      do l = l_start, level_end-1
          do d = 1, n_domain(rank+1)
              height => sca_coeff(S_HEIGHT)%data(d)%elts
              wc_h => wav_coeff(S_HEIGHT)%data(d)%elts
              if (present(l_start0)) then
                  call apply_interscale_d2(IWT_inject_h_and_undo_update, grid(d), l, 0, 1) ! needs wc
              else
                  call apply_interscale_d2(IWT_inject_h_and_undo_update__fast, grid(d), l, 0, 1) ! needs wc
              end if
          end do
          if (l .gt. l_start) call update_bdry__finish(sca_coeff(S_VELO), l) ! for next outer velocity
          call update_bdry__start(sca_coeff(S_HEIGHT), l+1)
          do d = 1, n_domain(rank+1)
              if (advect_only) cycle
              velo => sca_coeff(S_VELO)%data(d)%elts
              wc_u => wav_coeff(S_VELO)%data(d)%elts
              if (present(l_start0)) then
                  call apply_interscale_d2(IWT_interpolate_u_outer_add_wc, grid(d), l, 0, 1) ! needs val
              else
                  call apply_interscale_d2(IWT_interpolate_u_outer, grid(d), l, 0, 1) ! needs val
              end if
              call apply_to_penta_d(IWT_interp_vel_penta, grid(d), l)
          end do
          call update_bdry__finish(sca_coeff(S_HEIGHT), l+1)
          call update_bdry__start(sca_coeff(S_VELO), l+1)
          do d = 1, n_domain(rank+1)
              height => sca_coeff(S_HEIGHT)%data(d)%elts
              wc_h => wav_coeff(S_HEIGHT)%data(d)%elts
              call apply_interscale_d(IWT_interp_wc_h, grid(d), l, 0, 0)
          end do
          call update_bdry__finish(sca_coeff(S_VELO), l+1)
          do d = 1, n_domain(rank+1)
              if (advect_only) cycle
              velo => sca_coeff(S_VELO)%data(d)%elts
              wc_u => wav_coeff(S_VELO)%data(d)%elts
              call apply_interscale_d(IWT_interpolate_u_inner, grid(d), l, 0, 0)
          end do
          if (l .lt. level_end-1) call update_bdry__start(sca_coeff(S_VELO), l+1) ! for next outer velocity
          sca_coeff(:)%bdry_uptodate = .False.
      end do
  end subroutine

  real(8) function interp_outer_u(dom, i, j, e, offs, dims, i_chd, j_chd, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i
      integer j
      integer e
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,N_BDRY + 1) :: dims
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer ide
      real(8) wgt(9)
      ide = idx(i_chd+end_pt(1,2,e+1),j_chd+end_pt(2,2,e+1),offs_chd,dims_chd)
      wgt = Iu_Base_Wgt + dble(dom%I_u_wgt%elts(ide+1)%enc)
      interp_outer_u = sum(wgt* &
              (/velo(idx(i, j, offs, dims)*EDGE + e + 1), &
                velo(ed_idx(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 2 + 1), offs, dims) + 1), &
                velo(ed_idx(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 3 + 1), offs, dims) + 1), &
                velo(ed_idx(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 5 + 1), offs, dims) + 1), &
                velo(ed_idx(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 0 + 1), offs, dims) + 1), &
                velo(ed_idx(i + opp_no(1,1,e+1), j + opp_no(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 1 + 1), offs, dims) + 1) - &
                velo(ed_idx(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 2 + 1), offs, dims) + 1), &
                velo(ed_idx(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 3 + 1), offs, dims) + 1) - &
                velo(ed_idx(i + opp_no(1,1,e+1), j + opp_no(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 4 + 1), offs, dims) + 1), &
                velo(ed_idx(i + opp_no(1,2,e+1), j + opp_no(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 4 + 1), offs, dims) + 1) - &
                velo(ed_idx(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 5 + 1), offs, dims) + 1), &
                velo(ed_idx(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 0 + 1), offs, dims) + 1) - &
                velo(ed_idx(i + opp_no(1,2,e+1), j + opp_no(2,2,e+1), &
                hex_sides(:,hex_s_offs(e+1) + 1 + 1), offs, dims) + 1)/))
  end function

  subroutine cpt_vel_wc_penta(dom, p, c, offs, dims)
      type(Domain) dom
      integer p
      integer c
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,9) :: dims
      integer p_chd
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,9) :: dims_chd
      integer id_chd
      integer idN_chd
      real(8) v
      integer idE_chd
      real(8), dimension(2) :: u
      p_chd = dom%patch%elts(p+1)%children(c-4)
      if (p_chd .eq. 0) return
      call get_offs_Domain(dom, p_chd, offs_chd, dims_chd)
      if (c .eq. IMINUSJPLUS) then
          id_chd = idx(0, LAST - 1, offs_chd, dims_chd)
          idN_chd = idx(0, LAST, offs_chd, dims_chd)
          v = -(Iu_Base_Wgt(8) + dble(dom%I_u_wgt%elts(idN_chd+1)%enc(8)))*( &
                  velo(idx(0, PATCH_SIZE, offs, dims)*EDGE + UP + 1) &
                 +velo(idx(-1, PATCH_SIZE, offs, dims)*EDGE + RT + 1))
          if (dom%mask_u%elts(EDGE*id_chd+UP+1) .ge. ADJZONE) then
              wc_u(EDGE*id_chd+UP+1) = wc_u(EDGE*id_chd+UP+1) - v
              wc_u(EDGE*idN_chd+UP+1) = wc_u(EDGE*idN_chd+UP+1) + v
          end if
      else
          if (c .eq. IPLUSJMINUS) then
              id_chd = idx(LAST - 1, 0, offs_chd, dims_chd)
              idE_chd = idx(LAST, 0, offs_chd, dims_chd)
              v = (Iu_Base_Wgt(7) + dble(dom%I_u_wgt%elts(idE_chd+1)%enc(7)))*( &
                      velo(idx(PATCH_SIZE, 0, offs, dims)*EDGE + RT + 1) &
                     +velo(idx(PATCH_SIZE,-1, offs, dims)*EDGE + UP + 1))
              if (dom%mask_u%elts(EDGE*id_chd+RT+1) .ge. ADJZONE) then
                  wc_u(EDGE*id_chd+RT+1) = wc_u(EDGE*id_chd+RT+1) - v
                  wc_u(EDGE*idE_chd+RT+1) = wc_u(EDGE*idE_chd+RT+1) + v
              end if
          end if
      end if
      if (.not. c .eq. IJMINUS) then
          return
      end if
      id_chd = idx(0, 0, offs_chd, dims_chd)
      idN_chd = idx(0, 1, offs_chd, dims_chd)
      idE_chd = idx(1, 0, offs_chd, dims_chd)
      u = vel_interp_penta_corr(dom, offs, dims, offs_chd, dims_chd)
      if (dom%mask_u%elts(EDGE*id_chd+UP+1) .ge. ADJZONE) then
          wc_u(EDGE*id_chd+UP+1) = wc_u(EDGE*id_chd+UP+1) + u(1)
          wc_u(EDGE*idN_chd+UP+1) = wc_u(EDGE*idN_chd+UP+1) - u(1)
      end if
      if (dom%mask_u%elts(EDGE*id_chd+RT+1) .ge. ADJZONE) then
          wc_u(EDGE*id_chd+RT+1) = wc_u(EDGE*id_chd+RT+1) + u(2)
          wc_u(EDGE*idE_chd+RT+1) = wc_u(EDGE*idE_chd+RT+1) - u(2)
      end if
  end subroutine

  function vel_interp_penta_corr(dom, offs, dims, offs_chd, dims_chd)
      real(8), dimension(2) :: vel_interp_penta_corr
      type(Domain) dom
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,9) :: dims
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,9) :: dims_chd
      integer i
      integer j
      integer i_chd
      integer j_chd
      i = 0
      j = 0
      i_chd = 0
      j_chd = 0
      vel_interp_penta_corr = (/ &
              (Iu_Base_Wgt(9) + dble(dom%I_u_wgt%elts(idx__fast(i_chd+end_pt(1,2,UP+1), &
               j_chd+end_pt(2,2,UP+1), offs_chd(1)) + 1)%enc(9))) * &
              ((-velo(idx(0, -1, offs, dims)*EDGE + UP + 1) - &
               (-velo(idx(-1, -1, offs, dims)*EDGE + 1))) - &
              (velo(ed_idx(i + end_pt(1,1,UP+1), j + end_pt(2,1,UP+1), &
              hex_sides(:,hex_s_offs(UP+1) + 0 + 1), offs, dims) + 1) - &
              velo(ed_idx(i + opp_no(1,2,UP+1), j + opp_no(2,2,UP+1), &
              hex_sides(:,hex_s_offs(UP+1) + 1 + 1), offs, dims) + 1))), &
              (Iu_Base_Wgt(6) + dble(dom%I_u_wgt%elts(idx__fast(i_chd + end_pt(1,2,RT+1), j_chd + &
              end_pt(2,2,RT+1), offs_chd(1)) + 1)%enc(6)))* &
              (velo(idx(-1, -1, offs, dims)*EDGE + 1) + &
              velo(idx(-1, 0, offs, dims)*EDGE + RT + 1) - (velo(ed_idx(i + &
              opp_no(1,1,RT+1), j + opp_no(2,1,RT+1), &
              hex_sides(:,hex_s_offs(RT+1) + 1 + 1), offs, dims) + 1) - &
              velo(ed_idx(i + end_pt(1,1,RT+1), j + end_pt(2,1,RT+1), &
              hex_sides(:,hex_s_offs(RT+1) + 2 + 1), offs, dims) + 1)))/)
  end function

  real(8) function I_p(dom, var, id, id1, id2, id3, id4)
      type(Domain) dom
      real(8), pointer :: var(:)
      integer id
      integer id1
      integer id2
      integer id3
      integer id4
      I_p = (dom%overl_areas%elts(id+1)%a(1)*var(id1+1) + &
              dom%overl_areas%elts(id+1)%a(2)*var(id2+1) + &
              dom%overl_areas%elts(id+1)%a(3)*var(id3+1) + &
              dom%overl_areas%elts(id+1)%a(4)*var(id4+1))*dom%areas%elts(id+1)%hex_inv
  end function

  subroutine basic_F_restr_wgt(dom, i_par, j_par, e, offs_par, dims_par, i0, j0, &
          offs, dims, typ)
      type(Domain) dom
      integer i_par
      integer j_par
      integer e
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer i0, j0
      integer i, j
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,N_BDRY + 1) :: dims
      integer, dimension(2) :: typ
      integer, dimension(3,16) :: ije
      integer, dimension(2) :: ij_nbp_mp
      integer, dimension(2) :: ij_nbp_pp
      integer, dimension(2) :: ij_nbp_pm
      integer, dimension(2) :: ij_nbp_mm
      integer k, id_enc(4)
      integer, dimension(3) :: ije_lcsd
      real(8), dimension(6) :: wgt
      if (e .eq. UP) then
          id_enc = (/idx(i0-2,j0, offs, dims), idx(i0-2,j0+1, offs, dims), &
                     idx(i0+1,j0, offs, dims), idx(i0+1,j0+1, offs, dims)/)
      i = i0
      j = j0+1
      elseif (e .eq. RT) then
          id_enc = (/idx(i0  ,j0, offs, dims), idx(i0  ,j0+1, offs, dims), &
                     idx(i0+1,j0-2, offs, dims), idx(i0+1,j0-1, offs, dims)/)
      i = i0+1
      j = j0
      else
          write(0,*) 'Error 447: R_F_wgts'
          stop
      end if
      ije(:,UMZ+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 1 + 1)
      ije(:,UPZ+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 4 + 1)
      ije(:,WMP+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 0 + 1)
      ije(:,VPP+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 5 + 1)
      ije(:,WPM+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 3 + 1)
      ije(:,VMM+1) = (/i, j, 0/) + hex_sides(:,hex_s_offs(e+1) + 2 + 1)
      ij_nbp_mp = (/i, j/) + nghb_pt(:,hex_s_offs(e+1) + 0 + 1)
      ij_nbp_pp = (/i, j/) + nghb_pt(:,hex_s_offs(e+1) + 5 + 1)
      ij_nbp_pm = (/i, j/) + nghb_pt(:,hex_s_offs(e+1) + 3 + 1)
      ij_nbp_mm = (/i, j/) + nghb_pt(:,hex_s_offs(e+1) + 2 + 1)
      ije(:,VMP +1) = (/ij_nbp_mp(1), ij_nbp_mp(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 4 - 2 + 1)
      ije(:,VMPP+1) = (/ij_nbp_mp(1), ij_nbp_mp(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 1 + 4 + 1)
      ije(:,UZP +1) = (/ij_nbp_mp(1), ij_nbp_mp(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 0 + 4 + 1)
      ije(:,WPPP+1) = (/ij_nbp_pp(1), ij_nbp_pp(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 4 - 4 + 1)
      ije(:,WPP +1) = (/ij_nbp_pp(1), ij_nbp_pp(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 1 + 2 + 1)
      ije(:,VPM +1) = (/ij_nbp_pm(1), ij_nbp_pm(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 1 + 4 + 1)
      ije(:,VPMM+1) = (/ij_nbp_pm(1), ij_nbp_pm(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 4 - 2 + 1)
      ije(:,UZM +1) = (/ij_nbp_pm(1), ij_nbp_pm(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 3 - 2 + 1)
      ije(:,WMMM+1) = (/ij_nbp_mm(1), ij_nbp_mm(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 1 + 2 + 1)
      ije(:,WMM +1) = (/ij_nbp_mm(1), ij_nbp_mm(2), 0/) + hex_sides(:,hex_s_offs(e+1) + 4 - 4 + 1)
      k = 1
      if (dist(dom%ccentre%elts(tri_idx(i_par,j_par,adj_tri(:,k+1,e+1),offs_par, dims_par)+1), &
                    dom%ccentre%elts(tri_idx(ije(1,UZP+1),ije(2,UZP+1), &
                            adj_tri(:,-k+2,ije(3,UZP+1)+1),offs,dims)+1)) .lt. eps()) then ! COINSIDE
          dom%R_F_wgt%elts(id_enc(1)+1)%enc = 0.0_8
          dom%R_F_wgt%elts(id_enc(2)+1)%enc = 0.0_8
      else
          if (typ(k+1) .eq. OUTER1) then
              ije_lcsd = ije(:,VPP+1)
          else if (typ(k+1) .eq. OUTER2) then
              ije_lcsd = ije(:,WMP+1)
          else ! INSIDE
              ije_lcsd = ije(:,UZP+1)
          end if
          wgt = interp_F_wgts(e, k, ije_lcsd, dom%ccentre%elts(tri_idx(i_par, &
                  j_par, adj_tri(:,k+1,e+1), offs_par, dims_par) + 1), ije, &
                  (/VPP, WMP, UZP, UPZ, WPP, VMP, UMZ, WPPP, VMPP/))
          if (e .eq. RT) then
              wgt = (/wgt(2), wgt(3), wgt(1), wgt(5), wgt(6), wgt(4)/)
          else if (e .eq. UP) then
              wgt = (/wgt(3), wgt(1), wgt(2), wgt(6), wgt(4), wgt(5)/)
          end if
          dom%R_F_wgt%elts(id_enc(1)+1)%enc = wgt(1:3)
          dom%R_F_wgt%elts(id_enc(2)+1)%enc = wgt(4:6)
      end if
      k = 0
      if (dist(dom%ccentre%elts(tri_idx(i_par,j_par,adj_tri(:,k+1,e+1),offs_par, dims_par)+1), &
                    dom%ccentre%elts(tri_idx(ije(1,UZM+1),ije(2,UZM+1), &
                            adj_tri(:,-k+2,ije(3,UZM+1)+1),offs,dims)+1)) .lt. eps()) then ! COINSIDE
          dom%R_F_wgt%elts(id_enc(3)+1)%enc = 0.0_8
          dom%R_F_wgt%elts(id_enc(4)+1)%enc = 0.0_8
      else
          if (typ(k+1) .eq. OUTER1) then
              ije_lcsd = ije(:,VMM+1)
          else if (typ(k+1) .eq. OUTER2) then
              ije_lcsd = ije(:,WPM+1)
          else ! INSIDE
              ije_lcsd = ije(:,UZM+1)
          end if
          wgt = interp_F_wgts(e, k, ije_lcsd, dom%ccentre%elts(tri_idx(i_par, &
                  j_par, adj_tri(:,k+1,e+1), offs_par, dims_par) + 1), ije, &
                  (/VMM, WPM, UZM, UMZ, WMM, VPM, UPZ, WMMM, VPMM/))
          if (e .eq. UP) then
              wgt = (/wgt(3), wgt(1), wgt(2), wgt(6), wgt(4), wgt(5)/)
          else if (e .eq. RT) then
              wgt = (/wgt(2), wgt(3), wgt(1), wgt(5), wgt(6), wgt(4)/)
          end if
          dom%R_F_wgt%elts(id_enc(3)+1)%enc = wgt(1:3)
          dom%R_F_wgt%elts(id_enc(4)+1)%enc = wgt(4:6)
      end if
  contains
      function interp_F_wgts(e, k1, ije_lcsd, endpt_o, ije, stencil)
          real(8), dimension(6) :: interp_F_wgts
          integer e
          integer k1
          integer, dimension(3) :: ije_lcsd
          type(Coord) endpt_o
          integer, dimension(3,16) :: ije
          integer, dimension(9) :: stencil
          integer id_tri
          type(Coord) x
          type(Coord) y
          real(8), dimension(6,6) :: G
          type(Coord) endpt
          real(8), dimension(6) :: b
          integer, dimension(6) :: ipiv
          integer info
          id_tri = tri_idx(ije_lcsd(1), ije_lcsd(2), &
                  adj_tri(:,-k1+2,ije_lcsd(3) + 1), offs, dims)
          call local_coord(dom%ccentre%elts(id_tri+1), &
                  dom%ccentre%elts(id_tri+1), dom%midpt%elts(ed_idx(0, 0, &
                  ije_lcsd, offs, dims) + 1), x, y)
          G(:,1) = coords_to_row(ije(:,stencil(1) + 1), x, y)
          G(:,2) = coords_to_row(ije(:,stencil(2) + 1), x, y)
          G(:,3) = coords_to_row(ije(:,stencil(3) + 1), x, y)
          G(:,4) = coords_to_row(ije(:,stencil(4) + 1), x, y) - &
                  coords_to_row(ije(:,stencil(5) + 1), x, y)
          G(:,5) = coords_to_row(ije(:,stencil(6) + 1), x, y) - &
                  coords_to_row(ije(:,stencil(7) + 1), x, y)
          G(:,6) = coords_to_row(ije(:,stencil(8) + 1), x, y) - &
                  coords_to_row(ije(:,stencil(9) + 1), x, y)
          endpt = endpt_o
          b = coords_to_row_perp((/dom%ccentre%elts(id_tri+1), endpt/), x, y)
          ipiv = 0
          info = 0
          call dgesv(6, 1, G, 6, ipiv, b, 6, info)
          interp_F_wgts = b
          return
      end function
      function coords_to_row_perp(coords, x, y)
          real(8), dimension(6) :: coords_to_row_perp
          type(Coord), dimension(2) :: coords
          type(Coord) x
          type(Coord) y
          type(Coord) midpt
          type(Coord) dirvec
          midpt = mid_pt(coords(1), coords(2))
          dirvec = cross(vector(coords(1), coords(2)), midpt)
          coords_to_row_perp = coords_to_rowd(midpt, dirvec, x, &
                  y)*dist(coords(1), coords(2))
      end function
      function coords_to_row(ije0, x, y)
          real(8), dimension(6) :: coords_to_row
          integer, dimension(3) :: ije0
          type(Coord) x
          type(Coord) y
          integer i
          integer j
          integer e
          type(Coord) endpt1
          type(Coord) endpt2
          real(8) pedlen
          type(Coord) midpt
          i = ije0(1)
          j = ije0(2)
          e = ije0(3)
          endpt1 = dom%node%elts(idx(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), &
                  offs, dims) + 1)
          endpt2 = dom%node%elts(idx(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), &
                  offs, dims) + 1)
          pedlen = dist(dom%ccentre%elts(tri_idx(i, j, adj_tri(:,1,e+1), offs, &
                  dims) + 1), dom%ccentre%elts(tri_idx(i, j, adj_tri(:,2,e+1), &
                  offs, dims) + 1))
          midpt = mid_pt(endpt1, endpt2)
          coords_to_row = coords_to_rowd(midpt, vector(endpt1, endpt2), x, &
                  y)*pedlen
      end function
  end subroutine

  subroutine cpt_velo_wc(dom, i_par, j_par, i_chd, j_chd, offs_par, dims_par, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      integer idNE_chd
      integer e
      integer id1
      integer id2
      real(8) u
      real(8), dimension(6) :: u_inner
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      do e = 1, EDGE
          id1 = idx(i_chd + end_pt(1,1,e), j_chd + end_pt(2,1,e), offs_chd, &
                  dims_chd)
          id2 = idx(i_chd + end_pt(1,2,e), j_chd + end_pt(2,2,e), offs_chd, &
                  dims_chd)
          if (dom%mask_u%elts(EDGE*id2+e) .lt. ADJZONE) cycle
          u = interp_outer_u(dom, i_par, j_par, e - 1, offs_par, dims_par, &
                  i_chd, j_chd, offs_chd, dims_chd)
          wc_u(EDGE*id2+e) = velo(EDGE*id2+e) - u
          wc_u(EDGE*id1+e) = u - velo(EDGE*id2+e)
      end do
      u_inner = I_u_inner(dom, i_par, j_par, offs_par, dims_par, i_chd, j_chd, &
              offs_chd, dims_chd, EDGE*idE_chd + UP, DG + EDGE*idE_chd, &
              EDGE*idNE_chd + RT, EDGE*idN_chd + RT, DG + EDGE*idN_chd, &
              EDGE*idNE_chd + UP)
      if (dom%mask_u%elts(EDGE*idE_chd+UP+1) .ge. ADJZONE) &
          wc_u(EDGE*idE_chd+UP+1) = velo(EDGE*idE_chd+UP+1) - u_inner(1)
      if (dom%mask_u%elts(DG+EDGE*idE_chd+1) .ge. ADJZONE) &
          wc_u(DG+EDGE*idE_chd+1) = velo(DG+EDGE*idE_chd+1) - u_inner(2)
      if (dom%mask_u%elts(EDGE*idNE_chd+RT+1) .ge. ADJZONE) &
          wc_u(EDGE*idNE_chd+RT+1) = velo(EDGE*idNE_chd+RT+1) - u_inner(3)
      if (dom%mask_u%elts(EDGE*idN_chd+RT+1) .ge. ADJZONE) &
          wc_u(EDGE*idN_chd+RT+1) = velo(EDGE*idN_chd+RT+1) - u_inner(4)
      if (dom%mask_u%elts(DG+EDGE*idN_chd+1) .ge. ADJZONE) &
          wc_u(DG+EDGE*idN_chd+1) = velo(DG+EDGE*idN_chd+1) - u_inner(5)
      if (dom%mask_u%elts(EDGE*idNE_chd+UP+1) .ge. ADJZONE) &
          wc_u(EDGE*idNE_chd+UP+1) = velo(EDGE*idNE_chd+UP+1) - u_inner(6)
  end subroutine

  subroutine init_wavelets()
      integer d
      integer num
      integer i
      call init_Float_Field(wav_coeff(AT_NODE), AT_NODE)
      call init_Float_Field(wav_coeff(AT_EDGE), AT_EDGE)
      do d = 1, size(grid)
          num = grid(d)%node%length
          call init(grid(d)%overl_areas, num)
          call init(grid(d)%I_u_wgt, num)
          do i = 1, num
              call init_Iu_Wgt(grid(d)%I_u_wgt%elts(i), Iu_Base_Wgt)
          end do
          call init(grid(d)%R_F_wgt, num)
          do i = 1, num
              call init_RF_Wgt(grid(d)%R_F_wgt%elts(i), (/0.0_4, 0.0_4, 0.0_4/))
          end do
          call init(wav_coeff(S_HEIGHT)%data(d), num)
          call init(wav_coeff(S_VELO)%data(d), EDGE*num)
      end do
  end subroutine

  subroutine get_overl_areas(dom, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd, e, area, typ)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer e
      real(8), dimension(8) :: area
      integer, dimension(2) :: typ
      integer i
      type(Coord), dimension(6) :: hex
      type(Coord), dimension(3,2) :: tri
      type(Coord) pt
      type(Coord) inters_pt0
      logical does_inters0
      type(Coord) inters_pt1
      logical does_inters1
      logical troubles
      area = 0.0_8
      typ = 0
      hex = (/ (dom%ccentre%elts(tri_idx(i_chd, j_chd, no_adj_tri(:,i + &
              hex_s_offs(-e+3) + 1), offs_chd, dims_chd) + 1), i = 0, 6-1) /)
      tri = reshape((/dom%ccentre%elts(tri_idx(i_par, j_par, bfly_tri(:,4,e+1), &
              offs_par, dims_par) + 1), dom%ccentre%elts(tri_idx(i_par, j_par, &
              adj_tri(:,2,e+1), offs_par, dims_par) + 1), &
              dom%ccentre%elts(tri_idx(i_par, j_par, bfly_tri(:,3,e+1), &
              offs_par, dims_par) + 1), dom%ccentre%elts(tri_idx(i_par, j_par, &
              bfly_tri(:,2,e+1), offs_par, dims_par) + 1), &
              dom%ccentre%elts(tri_idx(i_par, j_par, adj_tri(:,1,e+1), &
              offs_par, dims_par) + 1), dom%ccentre%elts(tri_idx(i_par, j_par, &
              bfly_tri(:,1,e+1), offs_par, dims_par) + 1)/), (/3, 2/))
      pt = dom%node%elts(idx(i_chd, j_chd, offs_chd, dims_chd) + 1)
      area(1) = triarea(hex(6), hex(1), pt)
      area(2) = triarea(hex(3), hex(4), pt)
      do i = 1, 2
          call arc_inters(tri(1,i), tri(2,i), hex(3*i-2), hex(3*i-1), &
                  inters_pt0, does_inters0, troubles)
          call arc_inters(tri(3,i), tri(2,i), hex(3*i), hex(3*i-1), inters_pt1, &
                  does_inters1, troubles)
          if (does_inters0 .and. does_inters1) then
              area(i+4) = triarea(inters_pt0, tri(2,i), hex(3*i-1))
              area(i+6) = triarea(tri(2,i), hex(3*i-1), inters_pt1)
              area(i+2) = area(i+4) + area(i+6)
              area(i) = area(i) + triarea(hex(3*i-2), inters_pt0, pt) + &
                      triarea(inters_pt0, pt, tri(2,i))
              area(-i+3) = area(-i+3) + triarea(inters_pt1, hex(3*i), pt) + &
                      triarea(tri(2,i), pt, inters_pt1)
              typ(-i+3) = INSIDE
          else
              if (.not. does_inters0 .and. .not. does_inters1) then
                  area(i+2) = 0.0_8
                  call arc_inters(tri(2,1), tri(2,2), hex(3*i-2), hex(3*i-1), &
                          inters_pt0, does_inters0, troubles)
                  call arc_inters(tri(2,2), tri(2,1), hex(3*i-1), hex(3*i), &
                          inters_pt1, does_inters1, troubles)
                  if (.not. does_inters0 .and. does_inters1) then
                      area(i) = area(i) + triarea(hex(3*i-2), hex(3*i-1), pt) + &
                              triarea(hex(3*i-1), inters_pt1, pt)
                      area(-i+3) = area(-i+3) + triarea(inters_pt1, hex(3*i), &
                              pt)
                      typ(-i+3) = OUTER2
                  else
                      if (does_inters0 .and. .not. does_inters1) then
                          area(i) = area(i) + triarea(hex(3*i-2), inters_pt0, &
                                  pt)
                          area(-i+3) = area(-i+3) + triarea(hex(3*i-1), &
                                  hex(3*i), pt) + triarea(inters_pt0, &
                                  hex(3*i-1), pt)
                          typ(-i+3) = OUTER1
                      else
write(0,*) 'ERROR: overlap area', dom%id, offs_chd(1), i_chd, j_chd, 'A', does_inters0, does_inters1
                      end if
                  end if
              else
write(0,*)'ERROR: overlap area', dom%id, offs_chd(1), i_chd, j_chd, 'B', does_inters0, does_inters1
              end if
          end if
      end do
      return
  end subroutine

  subroutine normalize2(q, u, v)
      real(8), dimension(2) :: q
      real(8) nrm
      real(8) u
      real(8) v
      nrm = sqrt(q(1)**2 + q(2)**2)
      u = q(1)/nrm
      v = q(2)/nrm
  end subroutine

  subroutine IWT_inject_h_and_undo_update__fast(dom, i_par, j_par, i_chd, j_chd, &
          offs_par, dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_par
      integer id_chd
      integer idE
      integer idNE
      integer idN2E
      integer id2NE
      integer idN
      integer idW
      integer idNW
      integer idS2W
      integer idSW
      integer idS
      integer id2SW
      integer idSE
      id_par = idx(i_par, j_par, offs_par, dims_par)
      ! locally filled, IWT reproduces previous value
      if (dom%mask_p%elts(id_par+1) .ge. TOLLRNZ) return
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idE = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      idN2E = idx(i_chd + 2, j_chd + 1, offs_chd, dims_chd)
      id2NE = idx(i_chd + 1, j_chd + 2, offs_chd, dims_chd)
      idN = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idW = idx(i_chd - 1, j_chd, offs_chd, dims_chd)
      idNW = idx(i_chd - 1, j_chd + 1, offs_chd, dims_chd)
      idS2W = idx(i_chd - 2, j_chd - 1, offs_chd, dims_chd)
      idSW = idx(i_chd - 1, j_chd - 1, offs_chd, dims_chd)
      idS = idx(i_chd, j_chd - 1, offs_chd, dims_chd)
      id2SW = idx(i_chd - 1, j_chd - 2, offs_chd, dims_chd)
      idSE = idx(i_chd + 1, j_chd - 1, offs_chd, dims_chd)
      height(id_chd+1) = height(id_par+1) - &
              (wc_h(idE+1)*dom%overl_areas%elts(idE+1)%a(1) + &
              wc_h(idNE+1)*dom%overl_areas%elts(idNE+1)%a(2) + &
              wc_h(idN2E+1)*dom%overl_areas%elts(idN2E+1)%a(3) + &
              wc_h(id2NE+1)*dom%overl_areas%elts(id2NE+1)%a(4) + &
              wc_h(idN+1)*dom%overl_areas%elts(idN+1)%a(1) + &
              wc_h(idW+1)*dom%overl_areas%elts(idW+1)%a(2) + &
              wc_h(idNW+1)*dom%overl_areas%elts(idNW+1)%a(3) + &
              wc_h(idS2W+1)*dom%overl_areas%elts(idS2W+1)%a(4) + &
              wc_h(idSW+1)*dom%overl_areas%elts(idSW+1)%a(1) + &
              wc_h(idS+1)*dom%overl_areas%elts(idS+1)%a(2) + &
              wc_h(id2SW+1)*dom%overl_areas%elts(id2SW+1)%a(3) + &
              wc_h(idSE+1)*dom%overl_areas%elts(idSE+1)%a(4))*dom%areas%elts(id_par+1)%hex_inv
  end subroutine

  subroutine IWT_inject_h_and_undo_update(dom, i_par, j_par, i_chd, j_chd, &
          offs_par, dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_par
      integer id_chd
      integer idE
      integer idNE
      integer idN2E
      integer id2NE
      integer idN
      integer idW
      integer idNW
      integer idS2W
      integer idSW
      integer idS
      integer id2SW
      integer idSE
      id_par = idx(i_par, j_par, offs_par, dims_par)
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      if (dom%mask_p%elts(id_chd+1) .eq. FROZEN) return ! FROZEN mask -> do not overide with wrong value
      idE = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      idN2E = idx(i_chd + 2, j_chd + 1, offs_chd, dims_chd)
      id2NE = idx(i_chd + 1, j_chd + 2, offs_chd, dims_chd)
      idN = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idW = idx(i_chd - 1, j_chd, offs_chd, dims_chd)
      idNW = idx(i_chd - 1, j_chd + 1, offs_chd, dims_chd)
      idS2W = idx(i_chd - 2, j_chd - 1, offs_chd, dims_chd)
      idSW = idx(i_chd - 1, j_chd - 1, offs_chd, dims_chd)
      idS = idx(i_chd, j_chd - 1, offs_chd, dims_chd)
      id2SW = idx(i_chd - 1, j_chd - 2, offs_chd, dims_chd)
      idSE = idx(i_chd + 1, j_chd - 1, offs_chd, dims_chd)
      height(id_chd+1) = height(id_par+1) - &
              (wc_h(idE+1)*dom%overl_areas%elts(idE+1)%a(1) + &
              wc_h(idNE+1)*dom%overl_areas%elts(idNE+1)%a(2) + &
              wc_h(idN2E+1)*dom%overl_areas%elts(idN2E+1)%a(3) + &
              wc_h(id2NE+1)*dom%overl_areas%elts(id2NE+1)%a(4) + &
              wc_h(idN+1)*dom%overl_areas%elts(idN+1)%a(1) + &
              wc_h(idW+1)*dom%overl_areas%elts(idW+1)%a(2) + &
              wc_h(idNW+1)*dom%overl_areas%elts(idNW+1)%a(3) + &
              wc_h(idS2W+1)*dom%overl_areas%elts(idS2W+1)%a(4) + &
              wc_h(idSW+1)*dom%overl_areas%elts(idSW+1)%a(1) + &
              wc_h(idS+1)*dom%overl_areas%elts(idS+1)%a(2) + &
              wc_h(id2SW+1)*dom%overl_areas%elts(id2SW+1)%a(3) + &
              wc_h(idSE+1)*dom%overl_areas%elts(idSE+1)%a(4))*dom%areas%elts(id_par+1)%hex_inv
  end subroutine

  subroutine cpt_height_wc(dom, i_par, j_par, i_chd, j_chd, offs_par, dims_par, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      integer idNE_chd
      integer id2N_chd
      integer id2E_chd
      integer id2S_chd
      integer id2W_chd
      integer id2NE_chd
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      id2N_chd = idx(i_chd, j_chd + 2, offs_chd, dims_chd)
      id2E_chd = idx(i_chd + 2, j_chd, offs_chd, dims_chd)
      id2S_chd = idx(i_chd, j_chd - 2, offs_chd, dims_chd)
      id2W_chd = idx(i_chd - 2, j_chd, offs_chd, dims_chd)
      id2NE_chd = idx(i_chd + 2, j_chd + 2, offs_chd, dims_chd)
      if (dom%mask_p%elts(idNE_chd+1) .ge. ADJZONE) &
          wc_h(idNE_chd+1) = height(idNE_chd+1) - I_p(dom, &
                  height, idNE_chd, id2NE_chd, id_chd, id2E_chd, id2N_chd)
      if (dom%mask_p%elts(idN_chd+1) .ge. ADJZONE) &
          wc_h(idN_chd+1) = height(idN_chd+1) - I_p(dom, &
                  height, idN_chd, id_chd, id2N_chd, id2W_chd, id2NE_chd)
      if (dom%mask_p%elts(idE_chd+1) .ge. ADJZONE) &
          wc_h(idE_chd+1) = height(idE_chd+1) - I_p(dom, &
                  height, idE_chd, id_chd, id2E_chd, id2NE_chd, id2S_chd)
  end subroutine

  subroutine IWT_interp_wc_h(dom, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,9) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,9) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      integer idNE_chd
      integer id2N_chd
      integer id2E_chd
      integer id2S_chd
      integer id2W_chd
      integer id2NE_chd
      !  invers height transform: interpolate and add wavelet coefficent
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      if (dom%mask_p%elts(id_chd+1) .eq. FROZEN) return ! FROZEN mask -> do not overide with wrong value
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      id2N_chd = idx(i_chd, j_chd + 2, offs_chd, dims_chd)
      id2E_chd = idx(i_chd + 2, j_chd, offs_chd, dims_chd)
      id2S_chd = idx(i_chd, j_chd - 2, offs_chd, dims_chd)
      id2W_chd = idx(i_chd - 2, j_chd, offs_chd, dims_chd)
      id2NE_chd = idx(i_chd + 2, j_chd + 2, offs_chd, dims_chd)
      height(idNE_chd+1) = I_p(dom, height, idNE_chd, id2NE_chd, &
              id_chd, id2E_chd, id2N_chd) + wc_h(idNE_chd+1)
      height(idN_chd+1) = I_p(dom, height, idN_chd, id_chd, &
              id2N_chd, id2W_chd, id2NE_chd) + wc_h(idN_chd+1)
      height(idE_chd+1) = I_p(dom, height, idE_chd, id_chd, &
              id2E_chd, id2NE_chd, id2S_chd) + wc_h(idE_chd+1)
  end subroutine

  subroutine local_coord(midpt, endpt1, endpt2, x, y)
      type(Coord) midpt
      type(Coord) endpt1
      type(Coord) endpt2
      type(Coord) x
      type(Coord) y0
      type(Coord) y
      x = direction(endpt1, endpt2)
      y0 = cross(x, midpt)
      y = normalize_Coord(y0)
      return
  end subroutine

  type(Iu_Wgt) function outer_velo_weights(dom, p, i0, j0, e0, offs, dims)
      type(Domain) dom
      integer p
      integer i0
      integer j0
      integer e0
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,N_BDRY + 1) :: dims
      type(Coord) x
      type(Coord) y
      real(8), dimension(9) :: weights
      integer k
      integer id
      real(8), dimension(6,6) :: G
      real(8), dimension(6) :: b
      integer, dimension(6) :: ipiv
      integer info
      call local_coord(dom%midpt%elts(idx(i0, j0, offs, dims)*EDGE + e0 + 1), &
              dom%node%elts(idx(i0 + end_pt(1,1,e0+1), j0 + end_pt(2,1,e0+1), &
              offs, dims) + 1), dom%node%elts(idx(i0 + end_pt(1,2,e0+1), j0 + &
              end_pt(2,2,e0+1), offs, dims) + 1), x, y)
      weights = 0.0_8
      do k = 1, 2
          id = idx(i0, j0, offs, dims)
          G = reshape((/0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, &
                  0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, &
                  0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, &
                  0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, &
                  0.0_8, 0.0_8, 0.0_8, 0.0_8/), (/6, 6/))
          G(:,1) = coords_to_row(i0, j0, (/0, 0/), (/0, 0, e0/), e0)
          G(:,2) = coords_to_row(i0, j0, end_pt(:,-k+3,e0+1), &
                  hex_sides(:,hex_s_offs(e0+1) + 2 + 3*k - 3 + 1), e0)
          G(:,3) = coords_to_row(i0, j0, end_pt(:,k,e0+1), &
                  hex_sides(:,(hex_s_offs(e0+1) + 3) - (3*k - 3) + 1), e0)
          G(:,4) = coords_to_row(i0, j0, opp_no(:,k,e0+1), &
                  hex_sides(:,hex_s_offs(e0+1) + 1 + 3*k - 3 + 1), e0) - &
                  coords_to_row(i0, j0, end_pt(:,k,e0+1), &
                  hex_sides(:,hex_s_offs(e0+1) + 2 + 3*k - 3 + 1), e0)
          G(:,5) = coords_to_row(i0, j0, end_pt(:,-k+3,e0+1), &
                  hex_sides(:,(hex_s_offs(e0+1) + 3) - (3*k - 3) + 1), e0) - &
                  coords_to_row(i0, j0, opp_no(:,k,e0+1), &
                  hex_sides(:,(hex_s_offs(e0+1) + 4) - (3*k - 3) + 1), e0)
          G(:,6) = coords_to_row(i0, j0, end_pt(:,k,e0+1), &
                  hex_sides(:,(hex_s_offs(e0+1) + 5) - (3*k - 3) + 1), e0) - &
                  coords_to_row(i0, j0, end_pt(:,-k+3,e0+1), &
                  hex_sides(:,hex_s_offs(e0+1) + 0 + 3*k - 3 + 1), e0)
          b = coords_to_rowd(mid_pt(dom%midpt%elts(EDGE*id+e0+1), &
                  dom%node%elts(idx2(i0, j0, end_pt(:,2,e0+1), offs, dims) + &
                  1)), vector(dom%node%elts(idx2(i0, j0, end_pt(:,1,e0+1), &
                  offs, dims) + 1), dom%node%elts(idx2(i0, j0, &
                  end_pt(:,2,e0+1), offs, dims) + 1)), x, y)
          ipiv = 0
          info = 0
          call dgesv(6, 1, G, 6, ipiv, b, 6, info)
          weights(1) = weights(1) + b(1)
          weights(2*k:2*k + 1) = weights(2*k:2*k + 1) + b(2:3)
          weights(2*k + 4:2*k + 5) = b(4:5)
          weights(-2*k+6) = weights(-2*k+6) + b(6)
          weights(-2*k+7) = weights(-2*k+7) - b(6)
      end do
      outer_velo_weights = Iu_Wgt(0.5_8*weights - Iu_Base_Wgt)
  contains
      function coords_to_row(i00, j00, n_offs1, n_offs2, e00)
          real(8), dimension(6) :: coords_to_row
          integer i00
          integer j00
          integer, dimension(2) :: n_offs1
          integer, dimension(3) :: n_offs2
          integer e00
          integer i
          integer j
          integer e
          type(Coord) endpt1
          type(Coord) endpt2
          i = i00 + n_offs1(1) + n_offs2(1)
          j = j00 + n_offs1(2) + n_offs2(2)
          e = n_offs2(3)
          endpt1 = get_coord(i + end_pt(1,1,e+1), j + end_pt(2,1,e+1), e00)
          endpt2 = get_coord(i + end_pt(1,2,e+1), j + end_pt(2,2,e+1), e00)
          coords_to_row = coords_to_rowd(mid_pt(endpt1, endpt2), vector(endpt1, &
                  endpt2), x, y)
          return
      end function
      type(Coord) function get_coord(i, j, e)
          integer i
          integer j
          integer e
          if (i .eq. -1) then
              if (j .eq. -1 .and. is_penta(dom, p, IJMINUS - 1)) then
                  if (e .eq. RT) then
                      get_coord = dom%node%elts(nidx(LAST_BDRY, 0, IMINUS, &
                              offs, dims) + 1)
                      return
                  else
                      if (e .eq. UP) then
                          get_coord = dom%node%elts(nidx(0, LAST_BDRY, JMINUS, &
                                  offs, dims) + 1)
                          return
                      end if
                  end if
              else
                  if (j .eq. PATCH_SIZE .and. is_penta(dom, p, IMINUSJPLUS - &
                          1)) then
                      get_coord = dom%node%elts(nidx(0, 1, JPLUS, offs, dims) + &
                              1)
                      return
                  else
                      get_coord = dom%node%elts(idx(i, j, offs, dims) + 1)
                      return
                  end if
              end if
          else
              if (i .eq. PATCH_SIZE .and. j .eq. -1 .and. is_penta(dom, p, &
                      IPLUSJMINUS - 1)) then
                  get_coord = dom%node%elts(nidx(1, 0, IPLUS, offs, dims) + 1)
                  return
              else
                  if (i .eq. PATCH_SIZE + 1 .and. j .eq. PATCH_SIZE + 1 .and. &
                          is_penta(dom, p, IJPLUS - 1)) then
                      get_coord = dom%node%elts(nidx(1, 0, IJPLUS, offs, dims) &
                              + 1)
                      return
                  else
                      get_coord = dom%node%elts(idx(i, j, offs, dims) + 1)
                      return
                  end if
              end if
          end if
      end function
  end function

  function coord2local(c, x, y)
      real(8), dimension(2) :: coord2local
      type(Coord) c
      type(Coord) x
      type(Coord) y
      coord2local = (/inner(c, x), inner(c, y)/)
  end function

  subroutine IWT_interpolate_u_outer_add_wc(dom, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer e
      integer id1
      integer id2
      integer id_par
      real(8) u
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      do e = 1, EDGE
          id1 = idx(i_chd + end_pt(1,1,e), j_chd + end_pt(2,1,e), offs_chd, &
                  dims_chd)
          id2 = idx(i_chd + end_pt(1,2,e), j_chd + end_pt(2,2,e), offs_chd, &
                  dims_chd)
          id_par = idx(i_par, j_par, offs_par, dims_par)
          u = interp_outer_u(dom, i_par, j_par, e - 1, offs_par, dims_par, &
                  i_chd, j_chd, offs_chd, dims_chd)
          velo(EDGE*id2+e) = u + wc_u(EDGE*id2+e)
          velo(EDGE*id1+e) = 2*velo(EDGE*id_par+e) - u + wc_u(EDGE*id1+e)
      end do
  end subroutine

  subroutine IWT_interpolate_u_outer(dom, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer e
      integer id1
      integer id2
      integer id_par
      real(8) u
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      do e = 1, EDGE
!         if (dom%mask_u%elts(EDGE*id_chd+e) .ge. ADJZONE) cycle
          id1 = idx(i_chd + end_pt(1,1,e), j_chd + end_pt(2,1,e), offs_chd, &
                  dims_chd)
          id2 = idx(i_chd + end_pt(1,2,e), j_chd + end_pt(2,2,e), offs_chd, &
                  dims_chd)
          id_par = idx(i_par, j_par, offs_par, dims_par)
          u = interp_outer_u(dom, i_par, j_par, e - 1, offs_par, dims_par, &
                  i_chd, j_chd, offs_chd, dims_chd)
          velo(EDGE*id2+e) = u + wc_u(EDGE*id2+e)
          velo(EDGE*id1+e) = 2*velo(EDGE*id_par+e) - u + wc_u(EDGE*id1+e)
      end do
  end subroutine

  subroutine set_RF_wgts(dom, p_chd, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer p_chd
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      integer idNE_chd
      real(8), dimension(8) :: area
      integer, dimension(2) :: typ
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      call get_overl_areas(dom, i_par, j_par, i_chd + 1, j_chd, offs_par, &
              dims_par, offs_chd, dims_chd, RT, area, typ)
      call init_Overl_Area(dom%overl_areas%elts(idE_chd+1), area)
      call basic_F_restr_wgt(dom, i_par, j_par, RT, & 
              offs_par, dims_par, i_chd, j_chd, offs_chd, dims_chd, typ)

      call get_overl_areas(dom, i_par, j_par, i_chd + 1, j_chd + 1, offs_par, &
              dims_par, offs_chd, dims_chd, DG, area, typ)
      call init_Overl_Area(dom%overl_areas%elts(idNE_chd+1), area)

      call get_overl_areas(dom, i_par, j_par, i_chd, j_chd + 1, offs_par, &
              dims_par, offs_chd, dims_chd, UP, area, typ)
      call init_Overl_Area(dom%overl_areas%elts(idN_chd+1), area)
      call basic_F_restr_wgt(dom, i_par, j_par, UP, & 
              offs_par, dims_par, i_chd, j_chd, offs_chd, dims_chd, typ)
  end subroutine

  subroutine set_WT_wgts(dom, p_chd, i_par, j_par, i_chd, j_chd, offs_par, &
          dims_par, offs_chd, dims_chd)
      type(Domain) dom
      integer p_chd
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer idN_chd
      integer idE_chd
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      dom%I_u_wgt%elts(idE_chd+1) = outer_velo_weights(dom, p_chd, i_par, &
              j_par, RT, offs_par, dims_par)
      dom%I_u_wgt%elts(id_chd+1) = outer_velo_weights(dom, p_chd, i_par, j_par, &
              DG, offs_par, dims_par)
      dom%I_u_wgt%elts(idN_chd+1) = outer_velo_weights(dom, p_chd, i_par, &
              j_par, UP, offs_par, dims_par)
  end subroutine

  subroutine restrict_u(dom, i_par, j_par, i_chd, j_chd, offs_par, dims_par, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i_par, j_par
      integer i_chd, j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd, idN_chd, idE_chd, idNE_chd
      integer id_par
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      id_par = idx(i_par, j_par, offs_par, dims_par)
      idN_chd = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idE_chd = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE_chd = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
if (dom%mask_u%elts(EDGE*id_chd+RT+1) .gt. 0) &
      sol(S_VELO)%data(dom%id+1)%elts(EDGE*id_par+RT+1) = &
              0.5_8*(sol(S_VELO)%data(dom%id+1)%elts(EDGE*id_chd+RT+1) + &
                     sol(S_VELO)%data(dom%id+1)%elts(EDGE*idE_chd+RT+1))
if (dom%mask_u%elts(EDGE*id_chd+DG+1) .gt. 0) &
      sol(S_VELO)%data(dom%id+1)%elts(DG+EDGE*id_par+1) = &
              0.5_8*(sol(S_VELO)%data(dom%id+1)%elts(DG+EDGE*idNE_chd+1) + &
                     sol(S_VELO)%data(dom%id+1)%elts(DG+EDGE*id_chd+1))
if (dom%mask_u%elts(EDGE*id_chd+UP+1) .gt. 0) &
      sol(S_VELO)%data(dom%id+1)%elts(EDGE*id_par+UP+1) = &
              0.5_8*(sol(S_VELO)%data(dom%id+1)%elts(EDGE*id_chd+UP+1) + &
                     sol(S_VELO)%data(dom%id+1)%elts(EDGE*idN_chd+UP+1))
  end subroutine

  subroutine check_p(dom, i_par, j_par, i_chd, j_chd, offs_par, dims_par, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer id_par
      integer idE
      integer idNE
      integer idN2E
      integer id2NE
      integer idN
      integer idW
      integer idNW
      integer idS2W
      integer idSW
      integer idS
      integer id2SW
      integer idSE
      real(8) ratio
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
      id_par = idx(i_par, j_par, offs_par, dims_par)
      idE = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      idN2E = idx(i_chd + 2, j_chd + 1, offs_chd, dims_chd)
      id2NE = idx(i_chd + 1, j_chd + 2, offs_chd, dims_chd)
      idN = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idW = idx(i_chd - 1, j_chd, offs_chd, dims_chd)
      idNW = idx(i_chd - 1, j_chd + 1, offs_chd, dims_chd)
      idS2W = idx(i_chd - 2, j_chd - 1, offs_chd, dims_chd)
      idSW = idx(i_chd - 1, j_chd - 1, offs_chd, dims_chd)
      idS = idx(i_chd, j_chd - 1, offs_chd, dims_chd)
      id2SW = idx(i_chd - 1, j_chd - 2, offs_chd, dims_chd)
      idSE = idx(i_chd + 1, j_chd - 1, offs_chd, dims_chd)
      ratio = (1.0_8/dom%areas%elts(id_chd+1)%hex_inv + &
              dom%overl_areas%elts(idE+1)%a(1) + &
              dom%overl_areas%elts(idNE+1)%a(2) + &
              dom%overl_areas%elts(idN2E+1)%a(3) + &
              dom%overl_areas%elts(id2NE+1)%a(4) + &
              dom%overl_areas%elts(idN+1)%a(1) + &
              dom%overl_areas%elts(idW+1)%a(2) + &
              dom%overl_areas%elts(idNW+1)%a(3) + &
              dom%overl_areas%elts(idS2W+1)%a(4) + &
              dom%overl_areas%elts(idSW+1)%a(1) + &
              dom%overl_areas%elts(idS+1)%a(2) + &
              dom%overl_areas%elts(id2SW+1)%a(3) + &
              dom%overl_areas%elts(idSE+1)%a(4))*dom%areas%elts(id_par+1)%hex_inv
  end subroutine

  subroutine restrict_p(dom, i_par, j_par, i_chd, j_chd, offs_par, dims_par, &
          offs_chd, dims_chd)
      type(Domain) dom
      integer i_par
      integer j_par
      integer i_chd
      integer j_chd
      integer, dimension(N_BDRY + 1) :: offs_par
      integer, dimension(2,N_BDRY + 1) :: dims_par
      integer, dimension(N_BDRY + 1) :: offs_chd
      integer, dimension(2,N_BDRY + 1) :: dims_chd
      integer id_chd
      integer id_par
      integer idE
      integer idNE
      integer idN2E
      integer id2NE
      integer idN
      integer idW
      integer idNW
      integer idS2W
      integer idSW
      integer idS
      integer id2SW
      integer idSE
      integer d
      id_chd = idx(i_chd, j_chd, offs_chd, dims_chd)
if (dom%mask_p%elts(id_chd+1) .eq. 0) return
      id_par = idx(i_par, j_par, offs_par, dims_par)
      idE = idx(i_chd + 1, j_chd, offs_chd, dims_chd)
      idNE = idx(i_chd + 1, j_chd + 1, offs_chd, dims_chd)
      idN2E = idx(i_chd + 2, j_chd + 1, offs_chd, dims_chd)
      id2NE = idx(i_chd + 1, j_chd + 2, offs_chd, dims_chd)
      idN = idx(i_chd, j_chd + 1, offs_chd, dims_chd)
      idW = idx(i_chd - 1, j_chd, offs_chd, dims_chd)
      idNW = idx(i_chd - 1, j_chd + 1, offs_chd, dims_chd)
      idS2W = idx(i_chd - 2, j_chd - 1, offs_chd, dims_chd)
      idSW = idx(i_chd - 1, j_chd - 1, offs_chd, dims_chd)
      idS = idx(i_chd, j_chd - 1, offs_chd, dims_chd)
      id2SW = idx(i_chd - 1, j_chd - 2, offs_chd, dims_chd)
      idSE = idx(i_chd + 1, j_chd - 1, offs_chd, dims_chd)
      d = dom%id+1
      sol(S_HEIGHT)%data(d)%elts(id_par+1) = sol(S_HEIGHT)%data(d)%elts(id_chd+1) + &
              (wav_coeff(S_HEIGHT)%data(d)%elts(idE+1)*dom%overl_areas%elts(idE+1)%a(1) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idNE+1)*dom%overl_areas%elts(idNE+1)%a(2) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idN2E+1)*dom%overl_areas%elts(idN2E+1)%a(3) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(id2NE+1)*dom%overl_areas%elts(id2NE+1)%a(4) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idN+1)*dom%overl_areas%elts(idN+1)%a(1) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idW+1)*dom%overl_areas%elts(idW+1)%a(2) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idNW+1)*dom%overl_areas%elts(idNW+1)%a(3) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idS2W+1)*dom%overl_areas%elts(idS2W+1)%a(4) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idSW+1)*dom%overl_areas%elts(idSW+1)%a(1) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idS+1)*dom%overl_areas%elts(idS+1)%a(2) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(id2SW+1)*dom%overl_areas%elts(id2SW+1)%a(3) + &
              wav_coeff(S_HEIGHT)%data(d)%elts(idSE+1)*dom%overl_areas%elts(idSE+1)%a(4))* &
              dom%areas%elts(id_par+1)%hex_inv
  end subroutine
end module wavelet_mod
