module gyre_mod
  use main_mod
  implicit none

  real(8), parameter :: C_dt_penal = 4
  real(8), parameter :: C_dt_advec = 1
  real(8), parameter :: C_dt_freq = 1.15
  real(8), parameter :: C_dt_visc = 0.1

  real(8), parameter :: delta_M_star = 100e3
  real(8), parameter :: radius_star       = 6.371e6
  real(8), parameter :: L_star       = 3.000e6
  real(8), parameter :: H_star       = 3.5729e3
  real(8), parameter :: Omega_star   = 5.7664e-7
  real(8), parameter :: tau_star     = 0.07163
  real(8), parameter :: rho_star     = 1.027e3
  real(8), parameter :: g_star       = 1.0076e-2
  
  real(8), parameter :: f0_star       = Omega_star
  real(8), parameter :: beta_star    = Omega_star/radius_star
  real(8), parameter :: U_Sv_star    = tau_star/(beta_star*H_star*L_star*rho_star)
  real(8), parameter :: U_BC_star    = (L_star/delta_M_star) * U_Sv_star
  real(8), parameter :: c_star       = sqrt(g_star*H_star)
  real(8), parameter :: nu_star      = beta_star * delta_M_star**3
  real(8), parameter :: Re           = U_BC_star * delta_M_star/nu_star
  real(8), parameter :: Rd_star      = c_star/f0_star
  real(8), parameter :: FrBC         = U_BC_star/c_star;
  real(8), parameter :: Ro           = U_Sv_star/(L_star*f0_star)

  ! Dimensional scaling
  real(8), parameter :: Ldim = L_star  ! Horizontal length scale 
  real(8), parameter :: Hdim = H_star   ! Vertical length scale
  real(8), parameter :: Udim = U_BC_star ! Velocity scale is boundary layer speed
  real(8), parameter :: Tdim = Ldim/Udim ! Time scale

  ! Non-dimensional quantities
  real(8), parameter :: delta_M    = delta_M_star / Ldim
  real(8), parameter :: U_BC       = U_BC_star / Udim       ! Velocity of boundary layer
  real(8), parameter :: H          = H_star/Hdim            ! Normalization for depth 
  real(8), parameter :: Lx         = L_star/Ldim           ! Length scale
  real(8), parameter :: U_Sv       = U_Sv_star / Udim       ! Velocity of gyre
  real(8), parameter :: nu         = nu_star * Tdim/Ldim**2 ! Gravity
  real(8), parameter :: c          = c_star/Udim            ! Wave speed
  real(8), parameter :: g          = c**2.0_8/H             ! gravitational constant
  real(8), parameter :: f0         = f0_star * Ldim/Udim
  real(8), parameter :: Rd         = Rd_star/Ldim

  real(8) :: csq 
  real(8) :: c_p

  real(8), parameter :: LAND = 1
  real(8), parameter :: SEA = 0

  integer  :: CP_EVERY 

  real(8) :: Hmin, eta, alpha, dx_min

  integer :: BATHY_PER_DEG, npts_chi, npts_topo
  real(4), allocatable :: bathy_data(:,:)
  integer, allocatable :: n_patch_old(:), n_node_old(:)

  logical const_bathymetry

  integer iwrite

  real(8), parameter :: WINDSTRESS_CONVERSION_FACTOR = 0.1_8 
  real(8), dimension(2,180,90) :: windstress_data
contains
  subroutine apply_initial_conditions()
      integer d
      do d = 1, size(grid)
          sol(S_HEIGHT)%data(d)%elts = 0
          sol(S_VELO)%data(d)%elts = 0
      end do
  end subroutine
  
   subroutine cpt_windstress(dom, i, j, offs, dims)
      type(Domain) dom
      integer i
      integer j
      integer e, grid_level
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,N_BDRY + 1) :: dims
      integer id, idE, idN, idNE
      id = idx(i, j, offs, dims)
      idN = idx(i, j + 1, offs, dims)
      idE = idx(i + 1, j, offs, dims)
      idNE = idx(i + 1, j + 1, offs, dims)

      dom%windstress%elts(EDGE*id+RT+1) = proj_vel(get_stress, dom%node%elts(id+1), dom%node%elts(idE+1))
      dom%windstress%elts(DG+EDGE*id+1) = proj_vel(get_stress, dom%node%elts(idNE+1), dom%node%elts(id+1))
      dom%windstress%elts(EDGE*id+UP+1) = proj_vel(get_stress, dom%node%elts(id+1), dom%node%elts(idN+1))
      dom%windstress%elts(id*EDGE+RT+1:id*EDGE+UP+1) = dom%windstress%elts(id*EDGE+RT+1:id*EDGE+UP+1) &
           * dom%len%elts(id*EDGE+RT+1:id*EDGE+UP+1) 
  end subroutine

  subroutine cpt_topo_penal(dom, i, j, offs, dims)
      type(Domain) dom
      integer i
      integer j
      integer e, grid_level
      integer, dimension(N_BDRY + 1) :: offs
      integer, dimension(2,N_BDRY + 1) :: dims
      integer id
      real(8) lon, lat
      real(8) s, t
      real(8) geo, M_chi, b, h, dx_local
      real(8), dimension(3) :: dx_primal, dx_dual

      id = idx(i, j, offs, dims)
      if (.not. penalize) then
          dom%topo%elts(id+1) = 1
          return
      end if
      
      do e = 1, 3
         dx_primal(e) = dom%len%elts(id*EDGE+e)
         dx_dual(e) = dom%pedlen%elts(id*EDGE+e)
      end do
      dx_local = max(maxval(dx_primal), maxval(dx_dual))
      if (dx_local.eq.0.0_8) dx_local = dx_min

      call cart2sph(dom%node%elts(id+1), lon, lat)
      s = lon/MATH_PI*dble(180*BATHY_PER_DEG)
      t = lat/MATH_PI*dble(180*BATHY_PER_DEG)
      call smoothed_penal_topo(s, t, b, penal%data(dom%id+1)%elts(id+1), dx_local)
      if (const_bathymetry) b = 0
      dom%topo%elts(id+1) = 1.0_8 + b
  end subroutine

  subroutine smoothed_penal_topo(s0, t0, stopo, spenal, dx_smooth)
      real(8) s0, t0, stopo, spenal, dx_smooth
      integer s, t, is0, it0
      integer i, j
      real(8) chi_sum, topo_sum, sw_chi, sw_topo, M_chi, M_topo, r, wgt_chi, wgt_topo
      type(Coord) :: p, q

      p = proj_lon_lat(s0,t0)
      is0=nint(s0); it0=nint(t0)

      ! Smooth penalization mask
      if (npts_chi.eq.0) then ! no smooth
         if (bathy_data(is0,it0) > 0.0_8) then
            spenal = LAND
         else
            spenal = SEA
         end if
      else ! smooth
         sw_chi   = 0.0_8
         chi_sum  = 0.0_8
         do i = -npts_chi, npts_chi
            do j = -npts_chi, npts_chi
               s = is0+i ; t = it0+j
               call wrap_lonlat(s, t)
               q = proj_lon_lat(dble(s), dble(t))
               
               r = norm(vector(p,q))
               wgt_chi  = radial_basis_fun(r, npts_chi,  dx_smooth)
               
               if (bathy_data(s, t) > 0.0_8) then ! land
                  M_chi = LAND ! = 1
               else ! sea: geo < 0
                  M_chi = SEA ! = 0
               end if
               chi_sum  = chi_sum  + M_chi *wgt_chi
               sw_chi  = sw_chi  + wgt_chi
            end do
         end do
         spenal = chi_sum/sw_chi
      end if

      if (npts_topo.eq.0) then 
         if (bathy_data(is0,it0) > 0.0_8) then
            stopo = 0.0_8
         else
            stopo = (-min(bathy_data(s, t), -Hmin) -H_star)/Hdim
         end if
      else
         sw_topo  = 0.0_8
         topo_sum = 0.0_8
         do i = -npts_topo, npts_topo
            do j = -npts_topo, npts_topo
               s = is0+i ; t = it0+j
               call wrap_lonlat(s, t)
               q = proj_lon_lat(dble(s), dble(t))
               
               r = norm(vector(p,q))
               wgt_topo = radial_basis_fun(r, npts_topo, dx_smooth)
               
               if (bathy_data(s, t) > 0.0_8) then ! land
                  M_topo = 0.0_8
               else ! sea: geo < 0
                  M_topo = (-min(bathy_data(s, t), -Hmin) -H_star)/Hdim
               end if
               topo_sum = topo_sum + M_topo*wgt_topo
               sw_topo = sw_topo + wgt_topo
            end do
         end do
         stopo  = topo_sum/sw_topo
      end if
  end subroutine

  type(Coord) function proj_lon_lat(s,t)
    real(8) :: s, t
    real(8) :: lon, lat
    
    lon = s*MATH_PI/dble(180*BATHY_PER_DEG)
    lat = t*MATH_PI/dble(180*BATHY_PER_DEG)
    proj_lon_lat = project_on_sphere(sph2cart(lon, lat))
  end function proj_lon_lat  

  real(8) function radial_basis_fun(r, npts, dx_local)
      real(8) r, alph, dx_local
      integer :: npts
      alph = 1.0_8 / (npts/2 * dx_local)
            
      radial_basis_fun = exp(-(alph*r)**2)
  end function

 subroutine wrap_lonlat(s, t)
  ! longitude: wraparound allows for values outside [-180,180]
  ! latitude: works only if there is no cost at the pole
      integer s, t
      if (t .lt. lbound(bathy_data,2)) t = lbound(bathy_data,2) ! pole
      if (t .gt. ubound(bathy_data,2)) t = ubound(bathy_data,2) ! pole
      if (s .lt. lbound(bathy_data,1)) s = s + 360*BATHY_PER_DEG
      if (s .gt. ubound(bathy_data,1)) s = s - 360*BATHY_PER_DEG
  end subroutine

  subroutine finish_new_patches()
      integer d, p
      do d = 1, size(grid)
          do p = n_patch_old(d)+1, grid(d)%patch%length
              call apply_onescale_to_patch(cpt_topo_penal, grid(d), p-1, -2, 3)
              if (wind_stress) &
                  call apply_onescale_to_patch(cpt_windstress, grid(d), p-1, 0, 0)
          end do
      end do
  end subroutine
  real(8) function stress_by_ll(s_tau, lon, lat)
      integer s_tau
      real(8) lon, lat
      stress_by_ll = WINDSTRESS_CONVERSION_FACTOR*windstress_data(s_tau, &
                             modulo(nint(lon) + 90 - 1, 180) + 1, &
                             modulo(nint(lon) + 45 - 1, 90) + 1)
  end function
  real(8) function bilinear_interp(ll, lr, ul, ur, x, y)
      ! ul ur
      ! ll lr
      real(8) ll, lr, ul, ur, x, y
      bilinear_interp = ll*(1-x)*(1-y) + lr*x*(1-y) + ul*(1-x)*y + ur*x*y
  end function
  subroutine get_stress(lon, lat, u, v)
      real(8) lon, lat
      real(8) hlon, hlat
      real(8) flon, flat
      real(8) clon, clat
      real(8) u, v
      ! latitude  -90    0    90
      ! `hlat`    -44.5  0.5   45.5 
      ! `flat`    -45    0     45 
      ! `clat`    -46    1     46 
      ! f idx     180  180/2   180  = modulo(`flon` + 90 - 1, 180) + 1
      ! c idx      1  180/2+1   1   = modulo(`clon` + 90 - 1, 180) + 1

      ! longitude  -180    0    180
      ! `hlon`      -89.5  0.5   90.5 
      ! `flon`      -90    0     90 
      ! `clon`      -89    1     91 
      ! f idx       180  180/2   180  = modulo(`flon` + 90 - 1, 180) + 1
      ! c idx        1  180/2+1   1   = modulo(`clon` + 90 - 1, 180) + 1
      hlon = (lon+1)/2; flon = floor(hlon); clon = ceiling(lon)
      hlat = (lat+1)/2; flat = floor(hlat); clat = ceiling(lat)
      !  clat,flon  clat,clon
      !  flat,flon  flat,clon
      u = bilinear_interp(stress_by_ll(1,flon,flat), stress_by_ll(1,flon,clat), &
                          stress_by_ll(1,clon,flat), stress_by_ll(1,clon,clat), &
                          hlon-flon, hlat-flat)*cos(lat)
      v = bilinear_interp(stress_by_ll(2,flon,flat), stress_by_ll(2,flon,clat), &
                          stress_by_ll(2,clon,flat), stress_by_ll(2,clon,clat), &
                          hlon-flon, hlat-flat)*cos(lat)
  end subroutine

  subroutine write_and_export(k)
      integer l, k
      integer u, i
      call trend_ml(sol, trend)
      call pre_levelout()
      do l = level_start, level_end
          minv = 1.d63;
          maxv = -1.d63;
          u = 100000+100*k
          ! call write_level_mpi(write_primal, u+l, l, .True.)
          do i = 1, N_VAR_OUT
              minv(i) = -sync_max_d(-minv(i))
              maxv(i) = sync_max_d(maxv(i))
          end do
          if (rank .eq. 0) write(u,'(A, 4(E15.5E2, 1X), I3)') &
                  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ", minv, l
          if (rank .eq. 0) write(u,'(A, 4(E15.5E2, 1X), I3)') &
                  "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ", maxv, l
          u = 200000+100*k
          call write_level_mpi(write_dual, u+l, l, .False.)
      end do
      u = 100000+100*k
      close(u)
      call post_levelout()
      call barrier
      if (rank .eq. 0) call compress_files(k) 
  end subroutine

  subroutine gyre_dump(fid)
      integer fid
      write(fid) iwrite
  end subroutine

  subroutine gyre_load(fid)
      integer fid
      read(fid) iwrite
  end subroutine

  subroutine set_thresholds() ! quasi-geostrophic
      toll_height = U_SV*Ro*f0*Lx/grav_accel * threshold**(3.0_8/2.0_8)
      toll_velo   = U_SV*Ro       * threshold**(3.0_8/2.0_8)
  end subroutine
  
  subroutine read_test_case_parameters(filename)
      character(*) filename
      integer :: fid = 500
      character(255) varname
      open(unit=fid, file=filename, action='READ')
      read(fid,*) varname, max_level 
      read(fid,*) varname, threshold 
      read(fid,*) varname, optimize_grid 
      read(fid,*) varname, const_bathymetry 
      read(fid,*) varname, Hmin 
      read(fid,*) varname, eta 
      read(fid,*) varname, alpha 
      read(fid,*) varname, dt_write
      read(fid,*) varname, CP_EVERY
      read(fid,*) varname, time_end
      read(fid,*) varname, resume 
      read(fid,*) varname, BATHY_PER_DEG 
      read(fid,*) varname, npts_chi
      read(fid,*) varname, npts_topo
      if (rank.eq.0) then
         write(*,'(A,i3)')     "max_level        = ", max_level
         write(*,'(A,es11.4)') "threshold        = ", threshold
         write(*,'(A,i2)')     "optimize_grid    = ", optimize_grid 
         write(*,'(A,L3)')     "const_bathymetry = ", const_bathymetry
         write(*,'(A,es11.4)') "Hmin             = ", Hmin
         write(*,'(A,es11.4)') "eta              = ", eta
         write(*,'(A,es11.4)') "alpha            = ", alpha
         write(*,'(A,es11.4)') "dt_write         = ", dt_write
         write(*,'(A,i3)')     "CP_EVERY         = ", CP_EVERY
         write(*,'(A,es11.4)') "time_end         = ", time_end 
         write(*,'(A,i6)')     "resume           = ", resume
         write(*,'(A,i4)')     "BATHY_PER_DEG    = ", BATHY_PER_DEG
         write(*,'(A,i4)')     "npts_chi         = ", npts_chi
         write(*,'(A,i4)')     "npts_topo        = ", npts_topo
         write(*,*) ' '
      end if
      dt_write = dt_write * 60**2*24/Tdim
      time_end = time_end * 60**2*24/Tdim
      close(fid)
  end subroutine
  subroutine write_and_print_step()
      real(4) timing
      timing = get_timing()
      if (rank .eq. 0) write(1011,'(2(ES11.4, 1X), I3, 2(1X, I9), 1X, ES11.4)') &
              time, dt, level_end, n_active, timing
  end subroutine
end module
program gyre
  use main_mod
  use gyre_mod
  implicit none

  integer, parameter :: len_cmd_files = 12 + 4 + 12 + 4
  integer, parameter :: len_cmd_archive = 11 + 4 + 4
  character(len_cmd_files) cmd_files
  character(len_cmd_archive) cmd_archive
  character(9+len_cmd_archive) command1
  character(6+len_cmd_files) command2

  logical aligned
  integer k, ierr

  call init_main_mod()
  call read_test_case_parameters("gyre.in")

  wind_stress      = .True. 
  penalize         = .True.
  bottom_friction  = .False.

  iwrite = 0
  
  radius         = radius_star/Ldim
  grav_accel     = g
  omega          = omega_star * Ldim/Udim
  viscosity      = nu
  csq            = g*H
  friction_coeff = 3e-3_8

  dx_min = sqrt(4.0_8*MATH_PI*radius**2/(10.0_8*4**max_level+2.0_8)) ! Average minimum grid size
  kmax   = 2.0_8*MATH_PI/dx_min

  if (rank.eq.0) then
     write(*,'(A)') "Parameters of the simulation (dimensional)"
     write(*,'(A,es11.4)') "U_BC     = ", U_BC_star 
     write(*,'(A,es11.4)') "delta_M  = ", delta_M_star
     write(*,'(A,es11.4)') "c        = ", c_star
     write(*,'(A,es11.4)') "g        = ", g_star
     write(*,'(A,es11.4)') "Rd       = ", Rd_star
     write(*,'(A,es11.4)') "U_Sv     = ", U_Sv_star
     write(*,'(A,es11.4)') "nu       = ", nu_star
     write(*,'(A,es11.4)') "omega    = ", omega_star
     write(*,*) ' '
     write(*,'(A)') "Parameters of the simulation (non-dimensional)"
     write(*,'(A,es11.4)') "FrBC     = ", FrBC
     write(*,'(A,es11.4)') "Re       = ", Re
     write(*,'(A,es11.4)') "delta_M  = ", delta_M
     write(*,'(A,es11.4)') "U_BC     = ", U_BC
     write(*,'(A,es11.4)') "delta_M  = ", delta_M
     write(*,'(A,es11.4)') "c        = ", c
     write(*,'(A,es11.4)') "g        = ", g
     write(*,'(A,es11.4)') "Rd       = ", Rd
     write(*,'(A,es11.4)') "Ro       = ", Ro
     write(*,'(A,es11.4)') "U_Sv     = ", U_Sv
     write(*,'(A,es11.4)') "nu       = ", nu
     write(*,'(A,es11.4)') "omega    = ", omega
     write(*,'(A,es11.4)') "<dx_min> = ", dx_min
     write(*,*) ' '
  end if

  if (rank .eq. 0) write(*,*) 'Reading wind-stress data'
  open(unit=1087,file='wind_stress/7/taux')
  open(unit=1088,file='wind_stress/7/tauy')
  do k = lbound(windstress_data,3), ubound(windstress_data,3)
      read(1087,*) windstress_data(1,:,k)
      read(1088,*) windstress_data(2,:,k)
  end do
  close(1087)
  close(1088)

  if (.not. penalize) then
     const_bathymetry = .True.
     if (rank .eq. 0) write (*,*) 'running without bathymetry and continents'
  else
     ieta = 1.0_8/eta
     alpha_m1 = alpha - 1.0_8
  end if

  if (penalize) then
      if (rank .eq. 0) write(*,*) 'Reading bathymetry data'
      allocate(bathy_data(-180*BATHY_PER_DEG:180*BATHY_PER_DEG,-90*BATHY_PER_DEG:90*BATHY_PER_DEG))
      open(unit=1086,file='bathymetry')
      do k = ubound(bathy_data,2), lbound(bathy_data,2), -1 ! north to south (as read from file)
          read(1086,*) bathy_data(:,k)
      end do
      close(1086)
  end if
  call initialize(apply_initial_conditions, 1, set_thresholds, gyre_dump, gyre_load)

  if (rank .eq. 0) write (*,*) 'thresholds p, u:',  toll_height, toll_velo
  allocate(n_patch_old(size(grid)), n_node_old(size(grid)))
  n_patch_old = 2; call finish_new_patches()

  if (rank .eq. 0) write(*,*) 'Write initial values and grid'
  if (resume .lt. 0) call write_and_export(iwrite)

  if (rank.eq.0) then
     if (istep .eq. 0) then
        open(unit=1011,file='verlauf.out',form='formatted',STATUS='replace')
     else
        open(unit=1011,file='verlauf.out',form='formatted',STATUS='old', POSITION='append')
     end if
  end if

  do while (time .lt. time_end)
      call start_timing()

      n_patch_old = grid(:)%patch%length
      call time_step(dt_write, aligned)
      call finish_new_patches()

      call stop_timing()

      call write_and_print_step()

      if (rank .eq. 0) write(*,'(A,F11.5,A,F12.5,1(A,E13.5),A,I9)') &
              'time [d] =', time/3600.0_8/24.0_8*Tdim, &
              ', dt [m] =', dt*Tdim/60.0_8, &
              ', min. depth =', fd, &
              ', d.o.f. =', sum(n_active)
      if (aligned)  then
          iwrite = iwrite + 1
          call write_and_export(iwrite)
          if (modulo(iwrite,CP_EVERY) .ne. 0) cycle
          ierr = writ_checkpoint(gyre_dump)
          ! let all cpus exit gracefully if NaN has been produced
          ierr = sync_max(ierr)
          if (ierr .eq. 1) then ! NaN
              write(0,*) "NaN when writing checkpoint"
              call finalize()
              stop
          end if
          call restart_full(set_thresholds, gyre_load)
          deallocate(n_patch_old); allocate(n_patch_old(size(grid)))
          deallocate(n_node_old);  allocate(n_node_old(size(grid)))
          n_patch_old = 2; call finish_new_patches()
          ! finish_new_patches takes long time (the smoothing of penalization)
          ! barrier here so that this does not affect following timing
          call barrier()
      end if
  end do

  call finalize()
end program
